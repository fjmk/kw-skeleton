core = 7.x
api = 2

projects[kw_manifests][type] = module

projects[kw_manifests][download][type] = git
projects[kw_manifests][download][url] = git://github.com/kraftwagen/kw-manifests.git
projects[kw_manifests][subdir] = kraftwagen

projects[kw_itemnames][type] = module

projects[kw_itemnames][download][type] = git
projects[kw_itemnames][download][url] = git://github.com/kraftwagen/kw-itemnames.git
projects[kw_itemnames][subdir] = kraftwagen

projects[token][version] = 1.4
projects[token][subdir] = contrib

projects[entity][version] = 1.0
projects[entity][subdir] = contrib

projects[ctools][version] = 1.2
projects[ctools][subdir] = contrib

projects[features][version] = 2.0-beta1
projects[features][subdir] = contrib

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = contrib

projects[transliteration][version] = 3.1
projects[transliteration][subdir] = contrib

projects[pathauto][version] = 1.2
projects[pathauto][subdir] = contrib

projects[views][version] = 3.5
projects[views][subdir] = contrib

projects[devel][version] = 1.3
projects[devel][subdir] = contrib/devel

projects[diff][version] = 3.2
projects[diff][subdir] = contrib/devel

projects[search_krumo][version] = 1.3
projects[search_krumo][subdir] = contrib/devel

projects[coder][version] = 2.0-beta2
projects[coder][subdir] = contrib/devel
